#!/bin/bash

for subfolder in ./Data/[0-9]*; 
do 
	for csv_file in $subfolder/*.csv; 
		do mv "$csv_file" "${csv_file// /_}"; 
	done; 
done
