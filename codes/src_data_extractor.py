from time_processor import TimeStamp
from feature_generator import std_dev
from ref_data_extractor import start_time_of, end_time_of
from dataframe_util import *

def time(processed_src_data):
	time_column = processed_src_data.columns[0]
	for row in processed_src_data[time_column]:
		yield TimeStamp(time_in_sec=float(row))

def last_src_timestamp(processed_src_data):
	time_column = processed_src_data.columns[0]
	last_t = TimeStamp(time_in_sec=float(processed_src_data[time_column].iloc[-1]))

	return last_t


def data_column(processed_src_data, col_name):
	for row in processed_src_data[col_name]:
		yield row

def extract_src_data(processed_src_data, column, start, end, src_end_time):
	"""Extract data by column when start <= time <= end"""

	src_data = []
	start_index = int(round(start/TimeStamp(time_in_sec=0.02)))
	current_time = TimeStamp(time_in_sec=0.0)
	for current_time, data in zip(time(processed_src_data.iloc[start_index:]), processed_src_data[column].iloc[start_index:]):
		if start <= current_time <= end:
			src_data.append(data)
		elif current_time > end or src_end_time < start:
			break

	return src_data

def extract_derived_features(processed_src_data, ref_data, sampling_interval, src_end_time):
	"""
	Generates a row of derived features.
	Add new feature calculator and add it to list comprehension experssion if needed.
	"""
	derived_features = []
	new_data = []

	for column in processed_src_data.columns[1:]:
		# Extract a column of source data according to the sampling interval
		new_data = [extract_src_data(processed_src_data,\
									 column,\
									 sampling_interval[0],\
									 sampling_interval[1],\
									 src_end_time)]
		if new_data != [] and new_data[0] != []:
			derived_features.append(std_dev(new_data))

	return derived_features


def sampling_intervals(ref_data, sampling_elapse):
	# Ref data format:
	# set_no, elapse, start, end, confusion label
	# Divide time interval start-end into small chunks of length sampling_elapse

	return TimeStamp.split_time_interval(start_time_of(ref_data),\
										 end_time_of(ref_data),\
								         sampling_elapse)

def make_sampling_intervals(ref_data, sampling_elapse, valid_elapse=0.5):
	sampling_ints = []
	
	for row in ref_data.iterrows():
		# print(row[1])
		new_interval = sampling_intervals(row[1], sampling_elapse)
		
		if len(new_interval) > 0 and (new_interval[-1][1] - new_interval[-1][0]).toSecond() >= valid_elapse:
			sampling_ints += new_interval


	return sampling_ints

def split_column(df, col_name, sampling_ints, device_sampling_rate=61, **kwargs):
	start_index = 0
	columns = []

	for interval in sampling_ints:
		# Directly pull data using time is not reliable
		# We can translate time to index for each df.
		# starting from 0:00.00 -> 0, calculate time elapse between start and end
		# the tracker has a fixed sampling rate 61 hz. So end is 61*elapse+start

		start = interval[0].toSecond()
		end = interval[1].toSecond()
		end_index = device_sampling_rate*int(round(end-start)) + start_index
		new_col = slice_by_row(df[col_name], start_index, end_index)

		if len(new_col) >= 1:
			columns.append(new_col)
		else:
			if len(kwargs) > 0:
				print(kwargs['set_number'])
			print("NO DATA EXTRACTED")
			# t_diff = (interval[1] - interval[0]).toSecond()
			# if t_diff >= 1.0:
			# 	print('---------------------------------------------------------')
			# 	print("Invalid Data!")
			# 	print('Start time: '+str(interval[0]), 'End time: '+str(interval[1]))
			# 	print('Start index: '+str(start_index), 'End index: '+str(end_index))
			# 	print('Fetched data: ', new_col)
			# 	print('---------------------------------------------------------')
			# else:
			# 	print('---------------------------------------------------------')
			# 	print("Time difference: "+ str(t_diff))
			# 	print('---------------------------------------------------------')

		# 	if end - start <= 0.5 and start_index < end_index:
		# 		print('\n')
		# 		print('---------------------------------------------------------')
		# 		print("Invalid Data!")
		# 		print('Start time: '+str(interval[0]), 'End time: '+str(interval[1]))
		# 		print('Start index: '+str(start_index), 'End index: '+str(end_index))
		# 		print('Fetched data: ', new_col)
		# 		print('---------------------------------------------------------')
		# 		print('\n')

		# 	print("Skipped this interval. Just a place holder")

		start_index = end_index + 1

	print(columns)

	return columns