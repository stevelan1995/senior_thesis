# Author: Yupeng Lan

"""
This module provides APIs for frequently used operation in pandas dataframe.
Since dataframe operation is very perplexing,
I wrote this module to abstract away any specific usage
of dataframe operations and making them more literal.

Life is much easier with this! ^_^
Before writing this module, huge amount of time is wasted
trying out the correct usage of changing dataframe or creating 
new dataframe.

Special thanks to: StackOverFlow and Google.

Note:
ix is deprecated from pandas 0.20.0 and above. Use loc and iloc instead.

loc works on labels in the index.
iloc works on the positions in the index (so it only takes integers).
ix usually tries to behave like loc but falls back to behaving like iloc if the label is not in the index.
"""

import numpy as np
import pandas as pd
from pandas import DataFrame, Series

def append_df(dataframe, new_df, ignore_header=True, no_index=True):
	"""
	Append new values to dataframe if they have the same columns.
	If not, your dataframe might be messed up.
	"""

	if ignore_header:
		new_df = new_df.reset_index(drop=True)
		return pd.concat([dataframe, new_df], ignore_index=no_index)
	else:
		return pd.concat([dataframe, new_df], ignore_index=no_index)

def add_column(dataframe, col_name, values):
	"""
	This method appends a new column at the end of dataframe
	"""

	if col_name not in dataframe.columns:
		if type(values) == Series:
			values = values.to_list()
		if type(values) == DataFrame:
			values = values.ix[:, 0:1].values.tolist()
		if type(values) is list:
			values = list_to_df(values)

		dataframe[col_name] = values

	return dataframe

def series_to_list(series):
	return series.values.tolist()

def slice_by_row(dataframe, start = None, end = None):
	# Slice a dataframe, convert sliced Series data to DataFrame
	# fixme: make consisitent output format
	if start is None and type(end) is int:
		# This returns a Series
		return dataframe.iloc[:end]
	elif end is None and type(start) is int:
		# This is a Data frame
		return dataframe.iloc[start:]
	else:
		# This is a data frame
		return dataframe.iloc[start:end]

def slice_by_column(dataframe, start = None, end = None):
	# Slice a dataframe, convert sliced Series data to DataFrame
	# fixme: make consisitent output format
	if start is None and type(end) is int:
		# This returns a Series
		return dataframe.iloc[:,:end]
	elif end is None and type(start) is int:
		# This is a Data frame
		return dataframe.iloc[:,start:]
	else:
		# This is a data frame
		return dataframe.iloc[:,start:end]

"""
slice_row.....
.loc if you want to label index
.iloc if you want to positionally index.

pd.iloc[[0,2]]
equivalent to 
df.loc[df.index[[0, 2]];

The followings are all equivalent
df.ix[0:2, :] 

df.iloc[0:2, :]

df.iloc[0:2]

df[0:2]
"""
"""
#select or re arrange columns
source = ['price', 'date']
new_data = data[source]
print "re arrange column:"
print new_data
"""

def index_of(df, col_name, value):
	"""Return the first occurance of value"""
	if type(value) != str:
		value = str(value)
	
	matched_values = df[df[col_name] == value]
	matched_indexes = matched_values.index.tolist()
	print(matched_values, matched_indexes)
	# index_list = list(dataframe[dataframe[col_name] == value].index)
	if len(matched_indexes) > 0:
		print("at least one value is matched")
		index = matched_indexes[0]
	else:
		print("No match found")
		index = np.inf

	return index

def closest_match(df, col_name, value):
	"""
	FI
	"""
	pass

def merge_df_rows(dfs, ignore_header=True, ignore_index=True):
	"""
	Append a list of dataframes together
	"""

	combined_df = DataFrame()

	for df in dfs:
		combined_df = append_df(combined_df, df, ignore_header, ignore_index)

	return combined_df

def matched_rows(df, col_name, value):
	print(df[col_name], type(df[col_name]))
	return df[df[col_name] == value]

def merge_df_cols(df_cols):
	df = pd.concat(df_cols, axis=1)
	# return df

	return df

def to_type(df, type_name):
	pass

def match_str_values(regex, df, col_name,return_as_df=True):
	"""
	This method only works on pandas Series. Return a Dataframe that matches
	regex as default.

	regex must be a group pattern. For example, (5/\d{0,1}/2016)
	Your normal regex must be in a pair of parenthesis.
	"""
	try:
		matched_rows = df[col_name].str.extract(regex, expand=return_as_df).dropna()
	except TypeError as e:
		print('-------------------------------------')
		print("requested column must be a string!")
		print('You provided '+str(type(first_element_of(df[col_name]))))
		print('-------------------------------------')
		raise e

	return matched_rows


def list_to_df(val_list, col_name = None):
	"""
	Converts a python list to Dataframe
	"""

	if col_name is None:
		return pd.DataFrame(val_list)
	else:
		df_dict = {col_name: val_list}
		return pd.DataFrame(df_dict)

def split_by(key_value_list, col_name, dataframe):
	"""
	Split dataframe into a list of dataframe according to 
	column name and a list of key values
	"""
	return [matched_rows(key_value, col_name, dataframe) for key_value in key_value_list]

def nth_row(df, row_index):
	try:
		return df.iloc[row_index]
	except IndexError:
		print("-------------------------------------------")
		print("Check the length of parameter df!","df length: "+str(len(df)))
		print("Check your index!", "Index: " + str(row_index))
		if row_index > 0:
			print("df only has "+str(len(df))+ ", but you asked for "+str(row_index)+"th row.")
		else:
			print("df only has " + str(len(df)) + ", but you asked for " + str(len(df)+row_index) + "th row.")

		print("----------------------------------------")
		raise IndexError

def column(df, as_list=True, start_from=None):
	if as_list:
		col = df.columns.values.tolist()
	else:
		col = df.columns

	if start_from is not None:
		return col[start_from:]
	else:
		return col

def column_name_of(df, index):
	return column(df)[index]

def insert_column(df, index, column, col_name):
	"""
	Insert a new column in the dataframe at an arbitrary position
	"""

	if type(column) is list:
		column = list_to_df(column, col_name)
	
	df.insert(index, col_name, column)

	return df

def value_of(df, index):
	"""
	value_of assumes df is DataFrame or Series. Index refers to the index of row in df.
	size of row of df must be at least one and larger than index, unless index is less than 0.
	In that case, negative index is a syntatic sugar of getting df.iloc[len(df)-abs(index)], which is the last nth row of df.

	"""
	if type(df) is DataFrame or type(df) is Series:
		return nth_row(df, index)[0]
	else:
		print("-------------------------------------------")
		print("Passed df must be Series or DataFrame!")
		print("You provided a "+str(type(df)))
		print("-------------------------------------------")
		raise TypeError

def last_element_of(df):
	return value_of(df, len(df)-1)

def first_element_of(df):
	return value_of(df, 0)

def make_csv(feature_dict, path, keep_index=False):
	"""
	Turn an ordered dictionary to csv file
	"""
	print("Start writing data")

	output_df = DataFrame(feature_dict)
	if keep_index:
		output_df.to_csv(path)
	else:
		output_df.to_csv(path, index=False)

	print("Completed writing training data")	


if __name__ == '__main__':

	d = {'date':[1,2,3,4,5],
		'price':[32,84,48,56,23]}
	d2 = {'test':[6,7,8,9,10]}
	d3 = {'date':[6,7,8,9,10],
		'price':[24,25,26,27,28]}
	dl4 = [1,2,3,4,5]
	
	d5 = {'c1':[6,7,8,9,10],
		'c2':[24,25,26,27,28]}


	d6 = {'date':['5/1/2016', '5/2/2016','5/3/2016','5/7/2017','1/3/1015'],
		  'test':[1,2,3,4,5]}

	data = pd.DataFrame(d)
	data2 = pd.DataFrame(d2)
	data3 = pd.DataFrame(d3)
	data4 = pd.Series(dl4)
	data5 = pd.DataFrame(d5)
	data6 = pd.DataFrame(d6)

	# print data

	# # how to change a specific element value
	# print "first method"
	# data.set_value(1, 'date', 10)
	# print data

	# #method TWO
	# print "second method"
	# data['price'][3] = 10000
	# print data

	# #if you wanna add a column
	# print "add a column"
	# data['ER'] = [6,7,8,9,10]
	# print data
	# data = add_column(data, d2.keys()[0], data2)
	# print(data)

	# #if you want to return some value/column and remove from the list, use pop 
	# print "archive date and print DataFrame"
	# date = data.pop('date')
	# print date
	# print data

	# Slice data frame by column
	# print(slice_column(data, 1, 3))
	# print(index_of(data,'date',2))

	# Concatenate two dfs with same column name
	# combined_df = pd.concat([data, data3])
	# print(combined_df)

	# Build dataframe from list
	# print(data4)

	# Add column at arbitrary position
	# col_name = data2.columns.values.tolist()[0]
	# data.insert(2, col_name, data2)
	# print(data)
	# extracted_data = match_str_values('(5/\d{1,2}/2016)', data6)
	# extracted_data = match_str_values('(5/\d{1,2}/2016)', data6, col_name='date')
	# extracted_data = match_str_values('(5/\d{1,2}/2016)', data6, col_name='date',return_as_df=False)

	# print(extracted_data)

	print(value_of(dl4, -10))






