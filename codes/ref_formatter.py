# Convert raw text of time frames to formatted csv file

from pandas import read_csv
from time_processor import TimeStamp
from preprocessor import strip_fname_extension
from dataframe_util import *
from collections import deque

print("Dependencies loaded...")

def get_elapse(raw_df):
	""" Extract elapse from raw_df as a one column dataframe"""

	elapse_list = [value_of(raw_df, index) for index in xrange(1, len(raw_df), 3)]

	return list_to_df(elapse_list, 'Elapse')

def get_end_time(raw_df):
	""" Extract end time from raw_df as a one column dataframe"""

	end_time_list = [value_of(raw_df, index) for index in xrange(2, len(raw_df), 3)]

	return list_to_df(end_time_list, 'End_Time')

def get_start_time(raw_df):
	""" Make start time column by adding 0:00.000 at head and remove the last time stamp"""

	start_time_list = ['00:00.00']+[value_of(raw_df, index) for index in xrange(2, len(raw_df), 3)][:-1]

	return list_to_df(start_time_list, 'Start_Time')

def replace_head(processed_df):
	start_time_list = ['00:00.00']+[value_of(processed_df, index, 2) for index in xrange(1, len(processed_df))][:-1]

	return list_to_df(start_time_list, 'Start_Time')

def is_confusion(ts_start, ts_end, c_start, c_end):
	# Mark data as confusion if two time intervals overlap

	return c_start <= ts_start <= c_end or\
		   c_start <= ts_end <= c_end or\
		   ts_start <= c_start <= ts_end or\
		   ts_start <= c_end <= ts_end


# Get a list of set numbers using API in preprocessor
set_numbers = strip_fname_extension('../Data/raw_task_timeframe/', 'csv')
set_numbers = sorted([float(set_number) for set_number in set_numbers])

# load raw time frames
raw_timeframes = [read_csv('../Data/raw_task_timeframe/'+str(set_number)+'.csv', header=None) for set_number in set_numbers]

# make a set no column for each file
set_labels = [list_to_df([set_no for i in xrange(len(raw_tf)/3)], 'Set') for set_no, raw_tf in zip(set_numbers, raw_timeframes)]

# Read confusion data
confusion_df = read_csv('../Data/confusion.csv')

# Initialize confusion_labels as list of False values
confusion_label_list = [[False for i in xrange(len(set_label_df))] for set_label_df in set_labels]

# make elapse column, end column and start column respectively
elapse_cols = [get_elapse(raw_df) for raw_df in raw_timeframes] 
endtime_cols = [get_end_time(raw_df) for raw_df in raw_timeframes] 
starttime_cols = [get_start_time(raw_df) for raw_df in raw_timeframes]

# read confusion data for the current data set
# make a queue for these confusion intervals
confusion_groups = [df_to_list(df) for df in split_by(set_numbers, 'Set', confusion_df)]
confusion_queues = [deque(cf_group) for cf_group in confusion_groups]

row_index = 0
check_threshold = TimeStamp(time_in_sec=10.0)

for start_time_col, end_time_col, cf_q, confusion_labels in zip(starttime_cols, endtime_cols, confusion_queues, confusion_label_list):
	# Each column data correspond to one set of data
	# Most of set only has one confusion interval
	# Initialize confusion data as the first cf interval
	# if a set has multiple intervals, check and update if satisfy condition
	
	confusion_data = cf_q.popleft()
	set_number = confusion_data[0]
	c_start = TimeStamp(time_str=confusion_data[1])
	c_end = TimeStamp(time_str=confusion_data[2])

	start_time_col = df_to_list(start_time_col, flatten=True)
	end_time_col = df_to_list(end_time_col, flatten=True)

	row_index = 0
	for start_time, end_time in zip(start_time_col, end_time_col):
		# print(start_time, end_time)
		ts_start = TimeStamp(time_str=start_time)
		ts_end = TimeStamp(time_str=end_time)
		
		# If time frame overlaps confusion interval
		confusion_labels[row_index] = is_confusion(ts_start, ts_end, c_start, c_end)

		if ts_start > c_end:
			# queue is not empty
			if cf_q:
				confusion_data = cf_q.popleft()
				c_start = TimeStamp(time_str=confusion_data[1])
				c_end = TimeStamp(time_str=confusion_data[2])
			else:
				break

		row_index += 1

combined_sets = [merge_df_cols([set_label, elapse, start_time, end_time, list_to_df(confusion, 'Confusion')])\
					for set_label, elapse, start_time, end_time, confusion in\
					zip(set_labels, elapse_cols, starttime_cols, endtime_cols, confusion_label_list)]

combined_reference_data = merge_df_rows(combined_sets)
combined_reference_data.to_csv("../Data/reference_data.csv", index=False)

print("Finished generating reference data")