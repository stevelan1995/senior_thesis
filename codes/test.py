import pandas as pd
from dataframe_util import *
from pandas import read_csv
from time_processor import TimeStamp
from preprocessor import *
from src_data_extractor import *
from ref_data_extractor import *
from feature_generator import *

print("Dependencies loaded...")

ref_data_path = "../Data/reference_data.csv"
src_data_path = "../Data/src_data.csv"

ref_data = read_csv(ref_data_path)
src_data = read_csv(src_data_path)

sampling_elapse = TimeStamp(time_in_sec=5.0)

# Split src_data and ref_data according to set number
set_numbers = strip_fname_extension('../Data/raw_task_timeframe/', 'csv')
set_numbers = sorted([float(set_number) for set_number in set_numbers])

src_dfs = [matched_rows(set_no, 'Set', src_data) for set_no in set_numbers]
ref_dfs = [matched_rows(set_no, 'Set', ref_data) for set_no in set_numbers]

# print(ref_dfs)

feature_dfs = []

for source_data, reference_data, set_no in zip(src_dfs, ref_dfs, set_numbers): 
	sampling_interval_list = make_sampling_intervals(reference_data, sampling_elapse)
	feature_data_list = []
	# for interval in sampling_interval_list:
	# 	print(set_no, "Start: "+str(interval[0]), "End: "+str(interval[1]))
	feature_df = DataFrame()
	for column in source_data.ix[:,3:]:
		splited_columns = split_column(source_data, column, sampling_interval_list, set_number = set_no)
		
 		# In this case we only have one dataframe in the feature data list for each column
		new_feature = get_features(splited_columns, features=['std'], set_number=set_no)
		feature_data_list.append(new_feature)
	
	# print(feature_data_list)

	for features in feature_data_list:
		feature_df = merge_df_cols([feature_df]+features)
		print("Actual length comparison:", "feature:"+str(len(features[0])), "samp int: "+str(len(sampling_interval_list)), "ref: "+str(len(reference_data)))

	# Get confusion labels according to splited sampling intervals
	# Some data seems to be missing when labeling confusion. I don't know why. Have to artificially fill them
	# Probably not a good idea but it is the only hotfix for now.
	confusion_label = get_confusion_label(reference_data, sampling_interval_list, set_number=set_no)
	missed_length = len(features[0]) - len(confusion_label)
	confusion_label += [False for i in xrange(missed_length)]
	feature_df = add_column(feature_df, 'Confusion', confusion_label)
	feature_dfs.append(feature_df)


training_df = merge_df_rows(feature_dfs)
training_df.to_csv('./test.csv')