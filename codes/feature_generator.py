import math
import numpy as np
from pandas import DataFrame
from dataframe_util import *


def relative_mouse_vel(mouse_pos1, mouse_pos2, dt):
	"""
	Generate mouse velocity based on two mouse position in unit time frame
	This method will be a generator in the next version.
	Returns a tuple containing (mouse_vel_x, mouse_vel_y)
	"""
	dx = mouse_pos2[0] - mouse_pos1[1]
	dy = mouse_pos2[1] - mouse_pos1[1]

	return (dx/dt, dy/dt)

def relative_mouse_pos(src_dataset):
	"""
	Generates a tuple containing relative x, y coordinate of mouse position
	"""
	for row in src_dataset:
		row = row.item(0)
		yield mouse_pos_of(row)

def mouse_pos_of(row):
	"""
	Extract mouse position from a row of data
	"""
	mouse_pos_x = row.item(15)
	mouse_pos_y = row.item(16)

	return (mouse_pos_x, mouse_pos_y)

def time_diff(t1, t2):
	return t2 - t1

def average_point(x_coords, y_coords):
	"""
	Takes two columns containing data for x axis and y axis
	return the coordinate of the average point
	"""
	return (mean_of(x_coords), mean_of(y_coords))

def vector_length(x, y):
	return math.sqrt(x**2 + y**2)

def euclidian_distance(x_coords, y_coords):
	euclid_dists = [vector_length(x,y) for x,y in zip(x_coords.iterrows(), y_coords.iterrows())]
	return euclid_dists

def minimum_of(data):
	"""Find the minimum in the data. Assume data only has one column"""

	return data.min()[0]

def median_of(data):
	"""Find the median in the data. Assume data only has one column"""

	return data.median()[0] 

def mean_of(data):

	return data.mean()[0]

def std_dev(data_df):
	np_data_list = data_df.values
	std_dev = np.std(np_data_list)
	std_df = DataFrame({'std_'+column_name_of(data_df,0):[std_dev]})

	return std_df

fcs = {
	'std': std_dev
}

def get_features(data_batches, features=['std'], **kwargs):
	"""
	Unified driver method of get feature data
	kwargs takes addition parameters needed for feature calculators

	Currently, a dataframe is sufficient

	For each feature calculator, a batch of data is entered
	and turned into a combined feature dataframe from all iterations.

	ex. In one iteration, one batch of data gets into std feature calculator,
	then std will return a df with only one row with the name of 'std_'+ src_col_name
	"""
	feature_dfs = []


	for fc_key in features:
		# feature_list = [fcs[fc_key](data) for data in data_batches]
		feature_list = []
		feature_df= DataFrame()

		for data in data_batches:
			new_data = fcs[fc_key](data)
			feature_list.append(new_data)
			
			# if len(kwargs) > 0:
			# 	print("Set: "+str(kwargs['set_number']),"Data: "+str(data), "std: "+str(new_data))
			# else:				
			# 	print("Data: "+str(data), "std: "+str(new_data))
		# print(feature_list)
		feature_df = merge_df_rows(feature_list)
		feature_dfs.append(feature_df)
		# print(len(feature_df))
	# print(len(feature_dfs))
	return feature_dfs



