import pandas as pd
from pandas import DataFrame, read_csv
from time_processor import TimeStamp

def set_no_of(ref_data):
	return ref_data['Set']

def elapse_of(ref_data):
	return TimeStamp(time_str=ref_data[1])

def start_time_of(ref_data):
	return TimeStamp(time_str=ref_data[2])

def end_time_of(ref_data):
	return TimeStamp(time_str=ref_data[3])

def label_of(ref_data):
	return ref_data[4]

def get_confusion_label(ref_data, sampling_intervals, sampling_elapse = 5.0, **kwargs):
	"""
	Check whether sampling interval overlaps a time frame and whether
	the data is labeled as confusion
	"""
	confusion_label = []
	sampling_start = 0
	sampling_end =0
	for row in ref_data.iterrows():
		row = row[1]
		# print(row)
		elapse = elapse_of(row)

		if elapse.toSecond() >= 0.5:

			ref_start = start_time_of(row)
			ref_end = end_time_of(row)

			splitted_sample_size = int((ref_end-ref_start).toSecond())/int(sampling_elapse)
			remained_elapse = round(int((ref_end-ref_start).toSecond())%sampling_elapse,2)

			if remained_elapse >= 0.5:
				splitted_sample_size += 1

			sampling_end = sampling_start + splitted_sample_size

			if sampling_end < len(sampling_intervals):
				for sampling_interval in sampling_intervals[sampling_start:sampling_end]:
					label = label_of(row)
					confusion_label.append(label)
					# print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
	
					# if len(kwargs) > 0:
					# 	print("Set")
					# 	print(kwargs['set_number'])

					# print("Ref start: "+str(ref_start), "Ref end: "+str(ref_end), "s start: "+str(sampling_interval[0]), 's end: '+str(sampling_interval[1]), label)
					# print("sample size: "+str(splitted_sample_size), "remained: "+str(remained_elapse))
					# print("Sliced interval: "+str(sampling_start)+' - '+str(sampling_end))
					# print("REF size: "+str(len(ref_data)), "Sampling list range: " +str(0)+"-"+str(len(sampling_intervals)-1))
					# print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
	
				if sampling_intervals[sampling_end][0] >= ref_start:
					sampling_start = sampling_end
				else:
					sampling_start = sampling_end + 1



	return confusion_label