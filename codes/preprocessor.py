import os
import numpy as np
import pandas as pd
from time_processor import TimeStamp
from pandas import DataFrame, read_csv
from dataframe_util import *

print("Dependencies loaded")


# Note: Run in /code directory. Otherwise, the script does not work.


def extract_feature_columns(src_dataframe, feature_list, add_timestamp=True):
	"""
	Return a preprocessed data frame with only time and selected feature data in the feature list.
	"""
	if 'FPOGV' in feature_list and 'BPOGV' in feature_list:
		src_dataframe[feature][src_dataframe.FPOGV == 0].replace(0,np.nan).interpolate()
		src_dataframe[feature][src_dataframe.BPOGV == 0].replace(0,np.nan).interpolate()

	time_df = src_dataframe.iloc[:,3]


	feature_df = pd.concat([src_dataframe[feature].replace(0.0,np.nan).interpolate()\
								for feature in feature_list],\
								axis = 1, join_axes=[src_dataframe.index])
	# feature_df = pd.concat([time_df, feature_df], axis = 1, join_axes=[time_df.index])
	feature_df = insert_column(feature_df, 0, time_df, 'Time')

	if add_timestamp:
		# for tms in time_df.iteritems():
		# 	print(tms)
		ts_list = [str(TimeStamp(time_in_sec=round(tms[1],2))) for tms in time_df.iteritems()]
		ts_df = list_to_df(ts_list, 'time_stamp')
		feature_df = insert_column(feature_df, 0, ts_df, 'time_stamp')

	# print(feature_df)
	return feature_df


def combine_data(feature_list,
				 output_path = "../Data/",
				 gaze_data_dir = "../Data/src_data/gaze/",
				 gaze_set_ext = "_g.csv",
				 fix_data_dir = "../Data/src_data/fixation/",
				 fixation_set_ext = "_f.csv",
				 use_gaze=True):
	"""
	Note: Normally only run once unless need new feature to be included

	Read data by reading all the files in the src_data folder. 
	For each file, read as dataframe. 
	Then combine them together and write to hard disk.

	Feature list specifies what source features should be extracted from
	source data. I don't recommend using the full data set since the 
	original data is messy and cannot be formatted properly when combined
	together.
	"""
	print("Start combining source data")

	extension = ""

	if use_gaze:
		target_dir = gaze_data_dir
		extension = gaze_set_ext
		output_path += "training_data_all_gaze.csv"
	else:
		target_dir = fix_data_dir
		extension = fixation_set_ext
		output_path += "training_data_all_fixation.csv"

	set_numbers = []
	# traverse root directory, and list directories as dirs and files as files
	for root, dirs, files in os.walk(target_dir):
		# We now have multiple sets of data combined together
		# we need a extra column to distinguish them
		# In reference data, time frames must correspond to its set number
		set_numbers = sorted([float(f_name.strip(extension)) for f_name in files])

	# read data from disk
	src_dfs = [extract_feature_columns(
					read_csv(target_dir+str(set_number)+extension), feature_list
					) for set_number in set_numbers]

	# Combine source data
	combined_src_df = DataFrame()
	set_label_df = DataFrame()

	for set_no, src_df in zip(set_numbers, src_dfs):
		set_labels = [set_no for i in xrange(len(src_df))]
		new_set_label = list_to_df(set_labels)
		set_label_df = append_df(set_label_df, new_set_label)
		combined_src_df = append_df(combined_src_df, src_df)

	# Label each data set
	insert_column(combined_src_df, 0, set_label_df, 'Set')
	combined_src_df.to_csv(output_path, index=False)

	print("Finished combining source data")


def strip_fname_extension(path, file_extension, additional_name = ""):
	"""
	Return a list of set number based on file name
	Strip the file extension and additional strings provided in parameters
	"""
	set_numbers = []
	
	if '.' != file_extension[0]:
		file_extension = '.' + file_extension

	# traverse root directory, and list directories as dirs and files as files
	for root, dirs, files in os.walk(path):
		set_numbers = [f_name.strip(additional_name+file_extension) for f_name in files]
		# print(set_numbers)

	return set_numbers


def combine_data_by_set(root_path):
	pass



if __name__ == '__main__':
	feature_list = ['BPOGX','BPOGY','FPOGX','FPOGY','CX','CY']
	combine_data(feature_list)