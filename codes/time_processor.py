import re

class TimeStamp:
	"""Implement simple timestamp arithmetic operations"""
	tolerance = 0.0
	min_accepted_interval = 1.0

	def __init__(self, time_str=None,\
				 minute=None, second=None,millisec=None,\
				 time_in_sec=None):
		if time_str is not None:
			"""
			Takes a string in the form of mm:ss.0 or mm:ss.00
			"""
			format_str = "\d\d:\d\d\.(\d|\d\d)"
			format_matcher = re.compile(format_str)
			assert(format_matcher.match(time_str))
			self.min = int(time_str[:2])
			self.sec = int(time_str[3:5])
			if len(time_str) == 7:
				self.msec = int(time_str[6:7])*10
			elif len(time_str) == 8:
				self.msec = int(time_str[6:8])
		elif type(time_in_sec) is float:
			"""
			Takes a float number that represents time in seconds.
			For example: 43.76538, 87.51428
			"""
			self.min = int(time_in_sec) / 60
			
			if self.min > 0:
				self.sec = int(time_in_sec) - 60 * self.min
			else:
				self.sec = int(time_in_sec)
			
			self.msec = int(float('%.2g' % (time_in_sec - float(int(time_in_sec)))) * 100)
			
			if self.msec >= 100:
				self.msec -= 100
				self.sec += 1

				if self.sec >= 60:
					self.sec -= 60
					self.min += 1
		
		elif not(minute is None or second is None or millisec is None):
			assert((minute < 60) and (second < 60) and (millisec < 99))
			self.min = minute
			self.sec = second
			self.msec = millisec

		

	def minute(self):
		return self.min

	def second(self):
		return self.sec

	def millisec(self):
		return self.msec

	@staticmethod
	def __sub_ms(end, start):
		millsec_diff = end.millisec() - start.millisec()

		if millsec_diff < 0:
			millsec_diff += 100
			carry = 1
		else:
			carry = 0
		return (millsec_diff, carry)

	@staticmethod
	def __sub_sec(sec_end, sec_start, carry):
		sec_diff = sec_end.second() - sec_start.second() - carry

		if sec_diff < 0:
			sec_diff += 60
			carry = 1
		else:
			carry = 0

		return (sec_diff, carry)

	@staticmethod
	def __sub_minute(min_end, min_start, carry):
		return min_end.minute() - min_start.minute() - carry

	def __sub__(start, end):
		diff_millsec, carry = TimeStamp.__sub_ms(start, end)
		diff_sec, carry = TimeStamp.__sub_sec(start, end, carry)
		diff_minute = TimeStamp.__sub_minute(start, end, carry)

		time_diff = TimeStamp(minute=diff_minute, second=diff_sec, millisec=diff_millsec)

		return time_diff

	@staticmethod
	def __add_ms(t1, t2):
		ms_sum = t1.millisec() + t2.millisec()

		if ms_sum > 99:
			carry = 1
			ms_sum = ms_sum % 100
		else:
			carry = 0

		return (ms_sum, carry)

	@staticmethod
	def __add_sec(t1, t2, carry):
		sec_sum = t1.second() + t2.second() + carry
		if sec_sum >= 59:
			sec_sum = sec_sum % 60
			carry = 1
		else:
			carry = 0

		return (sec_sum, carry)

	@staticmethod
	def __add_min(t1, t2, carry):
		return t1.minute() + t2.minute() + carry

	def __add__(t1, t2):
		ms_sum, carry = TimeStamp.__add_ms(t1, t2)
		# print("ms:"+str(ms_sum), "carry:"+str(carry))
		sec_sum, carry = TimeStamp.__add_sec(t1, t2, carry)
		# print("sec:"+str(sec_sum), "carry:"+str(carry))
		minute_sum = TimeStamp.__add_min(t1, t2, carry)
		# print("minute:"+str(minute_sum))

		result = TimeStamp(minute=minute_sum, second=sec_sum, millisec=ms_sum)

		return result

	def __format_time(self, time_val):
		# Add 0 to timestamp value if it is less than 10
		if time_val < 10:
			return "0"+str(time_val)
		else:
			return str(time_val)


	def __str__(self):
		return self.__format_time(self.min)+":"+\
			   self.__format_time(self.sec)+"."+\
			   self.__format_time(self.msec)

	def __eq__(t1, t2):
		# exactly_equal = (t1.minute() == t2.minute() and\
		# 				 t1.second() == t2.second() and\
		# 				 t1.millisec() == t2.millisec())
		# almost_equal = t1.match(t2.toSecond())

		# return exactly_equal or almost_equal
		return t1.toSecond() == t2.toSecond()

	def __ne__(t1, t2):
		return t1.toSecond() != t2.toSecond()

	def __lt__(t1, t2):
		return t1.toSecond() < t2.toSecond()

	def __gt__(t1, t2):
		return t1.toSecond() > t2.toSecond()

	def __le__(t1, t2):
		return t1 == t2 or t1 < t2

	def __ge__(t1, t2):
		return t1 == t2 or t1 > t2

	def __div__(t1, t2):
		return t1.toSecond() / t2.toSecond()

	def __mod__(t1, t2):
		return t1.toSecond() % t2.toSecond()

	def toSecond(self):
		return float(60*self.min)+float(self.sec)+float(self.msec)/100.0

	@staticmethod
	def set_match_tolerance(tolerance_in_sec):
		TimeStamp.tolerance = tolerance_in_sec

	def match(self, t_in_sec):
		"""
		Decide whether time difference between self and the time in second form 
		is within tolerance
		"""
		# print("Ref Time: "+str(self.toSecond()),"src time: "+str(t_in_sec),0, "Difference: "+ str(abs(self.toSecond() - float(t_in_sec))), "Tolerance: "+str(TimeStamp.tolerance))
		isMatch = 0 <= abs(self.toSecond() - float(t_in_sec)) <= TimeStamp.tolerance
		# print("Match: "+str(isMatch))
		# return 0 <= abs(self.toSecond() - float(t_in_sec)) <= TimeStamp.tolerance
		return isMatch

	@staticmethod
	def __xrange_f_(start, end, step=1.0):
		# 
		remainder = (end - start) % step
		# print("End: "+ str(end), "Start: "+str(start), "Remainder: "+str(remainder))
		if remainder != 0.0:
			end -= remainder

		while start <= end:
			yield start
			start += step


	@staticmethod
	def split_time_interval(start, end, elapse_length):
		"""
		Divide time intervals according to elapse. If elapse_length * integer > end - start,
		leave the last underfull time elapse as it is.
		begin = start

		from begin to end, 
			
			interval.append((start, start+elapse_length))
		"""
		interval = []

		if end - start >= elapse_length:
			for t in TimeStamp.__xrange_f_(start.toSecond(), end.toSecond(), elapse_length.toSecond()):
				start_time = TimeStamp(time_in_sec=t)
				end_time = start_time + elapse_length
				
				if end_time <= end:
					interval.append((TimeStamp(time_in_sec=t), TimeStamp(time_in_sec=t) + elapse_length))
				else:
					interval.append((TimeStamp(time_in_sec=t), end))
		elif end - start >= TimeStamp(time_in_sec=TimeStamp.min_accepted_interval):
			# Allow under length time elapse to be included when it's larger than minimal time interval
			interval = [(start, end)]
		
		return interval


	@staticmethod
	def print_interval(time_interval):
		print("Start: "+str(time_interval[0]), "End: "+str(time_interval[1]))



if __name__ == '__main__':

	t1 = TimeStamp(time_str='00:40.5')
	print(str(t1))