# Convert a data frame to ordered dictionary

from collections import OrderedDict, defaultdict

def write_row(ordered_dict, values, label):
	for key, data in zip(ordered_dict.keys(), values):
		if ordered_dict[key] is None:
			ordered_dict[key] = [data]
		else:
			ordered_dict[key].append(data)

	# Assume label is the last key
	label_key = ordered_dict.keys()[-1]
	if ordered_dict[label_key] is None:
		ordered_dict[label_key]= [label]
	else:
		ordered_dict[label_key].append(label)