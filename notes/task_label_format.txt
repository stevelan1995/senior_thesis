##################################################################################
Generating automatic reference data from manually entered txt

To generate automatic labeling reference data, manually write file names and time frames into a txt
file. Then read the text line by line. 

If current line is a file name, check whether it exists.
If the file exists, write the file name and its path as a row.
Memorize the current file being processed. Start reading time frame.
Load the file being processed.
For each time frame under the current file, find the closest time frame that rounds to the manually imported time frame. Write that time frame into the reference data set (temporarily add the time into an ordered dictionary).

Keep reading the txt file and add time frame for each file until hitting end of line.
Convert dictionary to dataframe, and then write the dataframe to ref_dataset.csv

Separately export all gaze data and fixation data.
Separately train two models, one using all gaze data and another using fixations.

Note: Make sure to filter all data of which x >= 0.6

Format:

# Label data type	Can be: all_gaze, fixation
# Set_Number.Task_Number

# Time frame
start-end

Example:
~/Desktop/Capstone/ref_data/ref_data.txt
1:00-1:03

start_time - end_time

*************************************************************************************
Pseudo code:

import re
import os.path
from pandas import read_csv, DataFrame

label_ref_path = # some path to manually labeled time frame text file

with open(label_ref_path) as manually_labeled_timeframe:
	ref_data = manually_labeled_timeframe.readlines()

txt_path_matcher = re.compile("~/(([A-Z]*|[a-z]*|\d*)/+)(/[A-Z]+|[a-z]+|\d*)_(\d+).(txt)")
csv_path_matcher = re.compile("~/((^[A-Z]*|^[a-z]*|^[0-9]*)/+)(/^[A-Z]*|^[a-z]*|^[0-9]*|_+).(csv)")
time_matcher = re.compile("[0-9]+:[0-9]+\-[0-9]+:[0-9]+")

for line in ref_data:
	if txt_path_matcher.match(line) and os.path.exists(line):	# Investigate how to use RE match
		src_dataframe = read_csv(line)
	if time_matcher.match(line):
		# Round to the closest time frame
		# write time frame to output
*************************************************************************************

##################################################################################

##################################################################################
Handling automatic reference data

Automatic Labeling reference data is a csv file.

column: File_name
Time Frame: Real time frame from source data set.

##################################################################################

##################################################################################
Filter data using reference data

# Use a preset column name here.
column_names = [CNT	TIME(2017/02/19 16:54:10.596) FPOGX	FPOGY	FPOGS	FPOGD	FPOGID	FPOGV	BPOGX BPOGY	BPOGV	CX	CY	CS	USER	LPCX	LPCY	LPD	LPS	LPV	RPCX	RPCY	RPD	RPS	RPV	BKID	BKDUR	BKPMIN]

filtered_data_dict = OrderedDict([(#keys corresponding to columns,[]), ...])

for each row in reference data:
	file_path = row.item(0)
	src_dataframe = read_csv(file_path)
	src_data = src_dataframe.values[1:,]
	for each time_frame in row:
		slice data of rows relevant to feature calculation in src_data using time_frame
		filtered_data_dict = write_row(filtered_data_dict, sliced_data)

##################################################################################










