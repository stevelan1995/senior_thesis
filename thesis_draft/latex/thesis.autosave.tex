\documentclass[condensed]{union-cs-thesis}
% Condensed formating - Use \documentclass[condensed]{union-cs-thesis} to
% cause the layout to take fewer pages.  It uses single-spacing instead of
% double spacing, compresses the front matter, and removes the lists of
% figures and tables. This might be useful as you draft your thesis.

% Final formating - Use \documentclass[final]{union-cs-thesis} for the
% final version.

% Honors formating - Use \documentclass[honors]{union-cs-thesis} to
% make the document match the library's formating specifications for
% honors theses.

% COMPILING NOTE:
%
% To compile this file to produce a pdf document with all of the citations,
% references, and cross-references in place, use the following:
% 
%    latexmk -pdf thesis
%
% If you want less verbose output:
%
%    latexmk -pdf -silent thesis

\usepackage{graphicx}
\usepackage[noend]{algorithmic}
\usepackage[letterpaper,bindingoffset=\bindingOffset,%
  left=1in,right=1in,top=1in,bottom=1in,%
  footskip=.25in]{geometry}    


\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage[toc,page]{appendix}
\usepackage{indentfirst}
\usepackage{setspace}
\doublespacing
\usepackage[style=numeric, maxcitenames=2, backend=bibtex]{biblatex}
\addbibresource{thesis.bib}

\begin{document}
\pagenumbering{roman}

\title{Detect User Confusion Using Eye-Tracking and Machine Learning Techniques}
\author{Yupeng Lan}
\date{\today}

% The three commands below are only used when the honors option is
% passed to \documentclass.  
\authorLF{Yupeng Lan}
\dept{Department of Computer Science}
\advisor{Prof. Matthew Anderson}

\maketitle

\begin{abstract}
\makeabstract

Getting confused while using a software system is frustrating for users if no immediate help is available. I explored the possibility of building a machine learning model to identify confusion based on the method of a similar project done by Lalle et al. I designed an experiment that simulated the process of analyzing data collected from a social study, which includes data calculation, chart building, and data formatting. These tasks have various levels of difficulty, and two of the tasks are deliberately designed to confused participants. Participants followed the steps given in the instruction to complete the experiments. The steps in the instruction provide required actions with corresponding menu options or buttons in Excel. Confusion happens when the participant cannot map the step to the position of the corresponding menu option, and the participant must verbally report confusion. The time window of each step is taken during the experiment, and I also record the elapse of confusion event. A GazePoint GP3 eye-tracker is used to collect experiment data. Similar to the method used in Lalle et al.'s work, I divided the time windows into five seconds and summarized the geometric patterns of gaze points, fixations and cursor positions using standard deviation. The training data only included data containing confusion event in order to balance non-confusion and confusion classes. A wide range of machine learning algorithms were attempted to build a meaningful model. Support Vector Machine and Random Forest produced similar result over 747 feature data containing 333 confusion and 414 non-confusion. The classification accuracy of these two models were approximately 52\%. Since the classification rate is merely above 50\%, I conclude that no meaningful result is found using standard deviation of gaze points, fixations, and cursor positions. Future work should focus on developing more descriptive geometric features of gaze path and use nominal features to build more explanatory models.

\end{abstract}

\newpage

\tableofcontents
\listoffigures % empty if using condensed formatting
\listoftables % empty if using condensed formatting
\makepreamble

\pagenumbering{arabic}
\newpage


\section{Introduction} % \section* would create section without section
                       % number.

\indent
Confusion is one of the frustrating parts of user experience when users try to accomplish some tasks in complex software systems. Many studies have attempted to reduce the chance of making users confused before delivering the software system to users. However, it is still inevitable that users may get confused no matter how well the design of the user interface is. Some research projects have investigated the possibility of reducing confusion on the fly during the interaction between the user and the software system. \Textcite{Bosch:2015:ADL:2678025.2701397} . proposed a method to resolve confusion in an Intelligent Tutoring System. Based on \Textcite{Bosch:2015:ADL:2678025.2701397}, \Textcite{Lalle:2015:PUL:2678025.2701376} has conducted a study to show how to predict confusion when the user is interacting with a visualization-based interface. The study is a proof of concept of automating confusion detection. Like many other machine learning tasks, the data in \Textcite{Lalle:2015:PUL:2678025.2701376}’s research is unbalanced, which may decrease the effectiveness of the confusion identification model. In this project, I want to validate the idea of using machine learning algorithms to build confusion prediction model using a similar method.  

\par
Confusion can happen in many different kinds of scenarios. When completing a task using some software, the user needs to plan a set of concrete steps to operate on the user interface. If the user does not know the correct steps to complete the task, or the outcome is different from the user’s expectation, then the user will get confused. In this project, user confusion can be any one or both of the following cases: users cannot locate the functional component of the user interface corresponding to their plan; users find the software behaving differently from their expectations. In the first case, when the user cannot find a corresponding option in the user interface, the most direct reaction is to search for the desired menu option visually. In this case, the subject will exhibit many kinds of gaze patterns that are used to identify confusion. In the second case, when the user sees outcomes different from his or her expectation, he or she may undo the action and keep trying different approaches until meeting the expectation. In practice, users may exhibit a combination of visual search and undo previous actions. Thus, to make models to learn to identify confusion from eye movement pattern and the behavior of undoing past actions, I need to devise a set of features that highly correlate confusion with visual search and undoing behavior. 

\par
In this project, I hope to answer the question, "Can eye movement reveal confusion? If so, what eye movement features are the best indicator of confusion? Can models based on gaze feature outperform the one solely built on the frequency of undoing operations?"  My hypothesis is that geometric patterns such as the degree of spread can also predict confusion. To prove my hypothesis, I will adapt the data analysis methods used in Lalle et al.’s work. Then I will evaluate varieties of classification algorithms and select the model that produces the most accurate classification result. Lalle et al. applied the random forest algorithm to build a confusion prediction model. The result showed that combining a set of metrics will accurately predict confusion. Similarly, I applied Decision Tree based algorithms and Support Vector Machine to classify the data. I also evaluated other algorithms such as kstar algorithm. By comparing the classification result of these algorithms, I can evaluate the performance of models and find which algorithm(s) produces the best result.

\par
The performance of the gaze-based model and undo-based model will be evaluated and compared in the Data Evaluation Method. I designed an experiment to collect data for machine learning models. The basic approach is to let subjects complete eight common tasks in Excel and use a GazePoint GP3 eye tracker to record their eye movements. Subjects are required to report confusion verbally when getting confused. The Eye Tracking Experiment Section will provide more details about the experiment.

\par
The following sections are organized as follows: The background section will discuss Attentive User Interfaces, Adaptive User Interfaces, eye-tracking, and the meaning of eye movement patterns in cognition. The approach section will propose a method to answer the research question. The evaluation section contains metrics and methods to analyze experiment data and to draw conclusions. This report also includes a section of updated time line and a list of discontinued project topics with brief reasons of why they were interrupted.

\section{Related Work}\label{sec:related_work}

\Textcite{Lalle:2015:PUL:2678025.2701376} explored the possibility of using eye-tracking and machine learning to detect users' confusion. They concluded that the pupil size feature is the most important indicator of confusion using the Random Forest \parencite{JSSv028i05} algorithm. They also evaluated different combinations of feature set and concluded that a larger feature set can produce more accurate result than using a smaller feature set.

Fitts' Law \parencite{Fitts_Law} provides a guideline to help researchers predict the time the user needed to interact with a drop-down menu. 

\Textcite{Byrne:1999:ETV:302979.303118} studied how users interact with drop-down menu, and found that users primarily look from top to bottom, and they may skip a few items. If subjects cannot find their target, they will do back-tracing search. The backtracking pattern gives me the inspiration of using geometric patterns of gaze to classify confusion. In particular, if back-tracing happens frequently, then the gaze points should be more concentrated in the area of the menu where the participant looks for the target function.

\indent
\Textcite{hyrskykari} proposed a reading assistant software that assists non-native speakers reading foreign texts through eye-tracking. The system measured how long the reader was looking at a particular word, in another word, the duration of eye fixation, to determine whether the reader needs the definition of a particular word. In other words, the result of the study indicates the correlation between fixation and uncertainty. Thus, it is reasonable to include fixation as one of the features in my project. 

\section{Approach} \label{sec:approach}
The goal of this project is to find geometric patterns that indicates confusion. In particular the spread of gaze points, fixations, and cursor positions are evaluated.

\par
% Move detailed explanation to the dedicated sections
This project contains two stages: experiment and data analysis. In the experiment stage, I choose Microsoft Excel as the test platform. In the experiment instructions, each tasks contains sequential steps that correspond to particular menu options or buttons of the User Interface. The participants' task is to follow the instructions and find those menu options and buttons. Then they interact with those items on the User Interface to complete the task. I took the lap time of each step for each task. If subject cannot find the items of the instructed step after multiple attempts, then they need to self-report confusion verbally. Then I will mark the time frame of the step that the participant is working on as confusion. I collect the eye-tracking data, time frame data, and screen recording to prepare for data analysis. I will provide more details in the Experiment Section.

\par
In the data analysis stage, I wrote a data processing program using Python to turn unformatted raw data (including time intervals of each step and raw eye-tracking data) into processed training data. Then I feed the training data to WEKA to run machine learning algorithms. I will describe the details of data format, the algorithms of data processors, the algorithm of feature generator, and the structure of training data. The source code and data of this project is available on \url{https://stevelan1995@bitbucket.org/stevelan1995/senior_thesis.git.}.







\section{The Eye Tracking Experiment}\label{sec:experiment}
	\subsection{Experiment Design}
	% Desribe your experiment design and explain why you want to design your experiment in this way

	I designed eight Excel tasks that simulates the scenario of analyzing the data from a social study. The simulated social study investigates the population, marriage status, and church attendance of a small town of which population is 1466.
	
	\par
	% Explain why you want to use Excel
	% Remember to add citation later on!!
	I chose Microsoft Excel as the test platform because it has a complex User Interface, and large user population. Notice that the first 5 subjects performed the experiment on Office 2010 and other subjects did the experiment on the newer version of Excel from Office 365 due to a system upgrade. A lot of users, especially less experienced users, will get confused while using Excel. If the gaze patterns or other eye-related features are found to indicate confusion, then the result is generalizable than the result found on the test platform used in Lalle et al.'s project. They used a self-made Data Visualization software called ValueChart, which contains an interactive UI for visualizing preferences. The software is specifically designed for their research purposes. Even though the usability of the software was investigated in multiple related research, the user population is negligible compared with Microsoft Excel. In addition, their test platform focused on the confusion caused by not only the complexity of the User Interface but also the complexity of making preferential choices.

	\par
	% Include three screen shots of empty task
	% Describe and explain each task in detail along with images
	% or should I only pick one task as the example and show the spirit of the whole task
	% or should I divide them into 3 groups and explain one of each group. Use this method now.

	% Need citations on confusion

	% Give an overview of tasks first
	The primary purpose of these eight tasks is to simulate the procedure of mapping planned interaction to the location of the menu options or buttons and interact with them in correct order. The following scenario is a confusion event: when users plan a sequence of actions to interact with the User Interface to actuate some intentions, they cannot find the correct location of the corresponding menu options or buttons or the sequence of interactions is not correct after making several failed attempts. Thus, the instruction of each task has two parts: the goal of the task and the steps to complete the goal. 
	% Put a picture of one of the task here and explain it.
	% This part explains why I structure the instruction in this way
	For example, in the first task, I asked participants to calculate the total frequency of all categories of data. In the context of the simulated social study, the total frequency represents the total participants of the social study. This task simulates the case that the researcher wants to know how many participants participated the study so that the researcher can calculate the percentage for each class of subjects later on. Thus, the intention of this simulated researcher (the experiment subject) is to get the sum of the population in each data class. In order to actuate this intention, the subject needs to form a plan to interact with the User Interface: find the summation function in Excel, then select all data categories, and finally perform the calculation. Then the subject must map this plan to the specific location of menu options corresponding to each step of the plan. Thus, the subject must go through the following mental process in order to complete task 1. 

	% Put this in some algorithmetic description table 
	% The format is loose here... Organize them later. Get some advice
	% Put some screen shot that correspond to the pattern of visual search
	Step 1: Where to put the result of summation? 
	The subject reads the instruction, which tells the subject to look for the cell next to the cell named "Total"

	The corresponding reaction of the subject is to visually search for the cell named "Total" and then move the gaze right next that cell, which would find the correct position. The cursor will also follow the gaze and locate on the target cell once the subject has located the cell.

	Step 2: Where is the insert function button located?
	Step 3:	Where is the summation function located?
	Step 4: Where does the data range start and end?
	
	% Then I can differentiate task difficulty by adjusting the phrasing in the step descriptions
	I divided tasks into three groups by difficulties: easy, medium, hard. The difficulty level is manifested through controlling the number of required steps to complete the goal and the concreteness of instructed step. In order to let subjects fully exhibit the potential patterns in visual search, I deliberately avoided giving instructions on completing the task through short cuts. Notice that all tasks are likely to make participants confused, and the hardest tasks have the highest probability to confuse participants. Easy tasks give participants some confidence to start with, while medium and hardest tasks will challenge subjects and confuse them so that I can collect data and find differences in non-confusion gaze patterns and confusion gaze patterns.

	% Mention the pre-experiment exercise
	Before letting the participants do the actual tasks, I also give them a similar spreadsheet containing completed data and charts so that they can get familiar with the User Interface of Microsoft Excel, regardless of their familiarity and skill level. Another purpose of giving them this exercise material is to let them practice the confusion reporting protocol.

	% Mention about the change in confusion report protocol
	The very first version of the experiment design did not require the participants to verbally report confusion. Instead, I asked them to use the left key and right key of mice to report confusion, since the experiment control software of the eye-tracker can record mouse clicks. This protocol prevents eye movements of participants from being influenced by talking. However, the experiment control software is not always stable. It crashed several times while I was doing the experiment and lose all the recorded data. Even when the software work properly, it may not record mouse clicks. Thus, I have to let subjects verbally report confusion since this is a more reliable way.
	% Add citation of talking can influence eye movement



	\subsection{Experiment Setup}
	% Talk about Equipments devices, and preparation before the subject comes to the lab.
	
	% Mention the lab environment, put pictures here
	The lab environment is consists of two parts: the control area and the experiment room. The experiment room is on the left side of the control area. Both the control area and the experiment room have a screen that connects to a computer. Two sets of mice and keyboards are used, which one is for the experimenter, and the other is for the subject. The experiment room has a light switch to control the brightness in the room. In order to minimize the effect of lighting on the eye-tracker, I set the brightness of the room to the lowest level.
	% Talk about the parameters of GazePoint GP3
	I used a GazePoint GP3 eye tracker to collect eye data from participants. The eye tracker uses IR reflection from participants pupils to locate eye movements. The sampling rate of this eye-tracker is 60 Hz. It has 0.5 – 1 degree of visual angle accuracy. I attached more details about this eye tracker in the Appendix. % add to appendix.

	% Talk about the experiment control software
	% Provide two screen captures
	The experiment control software comes along with the eye tracker. The software has a data collector and a monitor program. The data collector manages experiment data and the status of the eye-tracking device. I use this data collector to initialize recording and do data exportation. The monitor program displays the real-time recordings captured by the eye tracker. The monitor program also performs calibration on subjects' head distance and the accuracy of eye-tracking.

	% Talk about why you use two sets of keyboards and mice
	Since the experimenter and the subject share the same computer, then both of them need to control the computer asynchronously. I installed two sets of mice and keyboard for such purpose. I prepare the experiment materials and control the experiment controlling program. After I start recording, I hand over the control to subjects so they can do the tasks without interference.
	
	% Talk about the version of Excel you used
	The version of Excel used on first 7 subjects was 2010. Due to a system upgrade, the version has been updated to the latest version (Office 365). Some changes in the User Interface happened after the upgrade. I made the experiment instruction according to the older version so the software upgrade brought some minor discordance to the instructions. Some subjects got confused because the steps did not completely match the UI components in the newer version.
	
	% Talk about the hardware of the lab computer
	The lab computer has an Intel i7 6700K CPU and 4 GB of memory before the upgrade. The RAM capacity increased to 8 GB after the upgrade. An upgrade was performed because the experiment control software requires huge amount of resources to operate properly. The software is very likely to crash and lose data when the memory runs out.
	
	% Mention the timer you used, from 
	I used a web-based lap timer to record step intervals.

	More details about the experiment setup is available in the Appendix.
	\subsection{Pre-Experiment Procedure}
	% Talk about calibration, consent, pre-experiment instruction, etc.
	
	% Talk about the materials I gave to participants when they come in
	I hand out the background questionnaire, the consent form, and the pre-experiment instruction when a participant comes to the lab. The consent form introduces the basic content of the experiment and asks whether the participant agrees to be the experiment subject. The subject is informed to be video recorded. The background questionnaire asks for the background of the participant, their age, their Excel skill level, their computer skill level, and their visual ability (whether they have visual impairment, e.g. nearsightedness). The pre-experiment instruction provides details of doing calibration and completing the pre-experiment exercise. The full content of the consent form, background questionnaire, and the pre-experiment instructions are available in the Appendix.
	
	% Talk about the calibration process
	% Stage 1 of calibration, attach image
	The first stage of calibration process adjust the head distance and pose from the screen and the eye tracker. I require each participant to calibrate their biometrics for the eye-tracker. The subject sits on a chair and adjust the height of the chair to find a comfortable position. I ask participants to find the position they are comfortable with so that they do not constantly move and adjust their body position during the experiment. Some participants may get too relaxed and reduce their body height, which will break the tracking of the eye tracker. To prevent that from happening, I adjust the angle of the eye tracker so that participants' eyes are positioned at the bottom half of the screen in the monitor program. Then I ask participants to adjust their head distances from the eye tracker. A dot will on the monitor program will move left if the participant is too close to the screen. The dot will move to the right if the participant is too far away. Whenever they change their head distance of height of the chair, I adjust the angle of the eye tracker to make sure the camera is directly facing the participants. When the dot is positioned roughly in the middle, then the head distance and the angle of the eye tracker is correctly calibrated.

	% Stage 2 of calibration, attach image, make a reference on the eye-tracking experiment book
	The second stage of calibration evaluates the accuracy of eye tracing by matching the device-inferred gaze with points on the screen. I perform a nine-point calibration on the participant. At the beginning of the experiment, a shrinking white circle with a red center appears on the top left of the screen. The participant need to fixate the sight on that point until it disappears. Then the point will reappear on the top center. Then the point will keep moving to the right until it reaches the down right corner of the screen. To see a complete procedure of eye tracking calibration, the appendix has screen shots showing the positions of the nine dots. After the calibration, I evaluate the accuracy of gaze tracing. In the evaluation mode, the monitor program displays nine circles of equal sizes with nine crosses as the centers of these circles. Ideally, the eye tracker will exactly show where the participant is looking. When the participant looks at another location on the screen, the gaze path will be displayed smoothly. If stable tracking cannot be established, gaze points will appear randomly. I point the mouse cursor to the center of the top left circle. The participant looks at where I point to. Then I move the mouse cursor from left to right, top to left until the participant has seen all the centers of the circle. If the gaze point almost matches the centers, then I will end the calibration session and move on the exercise stage. If not, I will repeat the calibration until the accuracy becomes acceptable.

	% Exercise Stage
	After the calibration process, I give each participant a sample spreadsheet to get familiar with the User Interface of Excel. Each participant has three minutes to practice. While the participant is trying out functions of Excel, I explain the confusion report process and functions involved in the real experiment. Each participant has three minutes to work on the exercise. I verbally give instructions on how they should interact with the User Interface. To let them practice data calculation, I ask them to click on the cells where data are calculated using various functions. Then they will see the formula that produce the result of calculation. To let participants get familiar with chart building, I let them enable the data selection options in the chart, and they can see the cells included in the chart. For cell formatting and data formatting, I use mouse cursor to point out the locations of the buttons related to these two tasks. After this step, I let them practice the confusion report protocol.

	
	\subsection{Formal Experiment}
	\par
	% Describe and explain how you time the steps and how you record confusion.
	% Explain what you did when subject gets confused

	After the warm up, the participant starts to do experiment tasks along with the instruction document. When the participant gets ready, I start timing their steps of each task and initialize eye tracking and screen recording simultaneously. Then I immediately hand over the control to the participant. 

		


\section{Data Processing}\label{sec:data_processing}
\par
The data analysis stage contains a cycle of three steps: raw data extraction, feature engineering, and model building in WEKA. The raw data consist of two parts: raw time frame data from a web-based timer and eye tracking data from the eye tracker. The raw time frame data is fed into reference data formatter to generate reference data. Each line of reference data designate the start location and end location to extract preprocessed source data and label the data as confusion if the confusion label column has true value. I applied a preprocessor to raw eye tracking data to select types of raw data I need, which in this case the selected raw data are gaze points, fixation points, and cursor positions. These data are sliced by a 5-second time windows used in Lalle et al.'s work. If any data set contains missed values, the preprocessor will use linear interpolation to fill the missed values. Once formatted both the reference data and source data, all the formatted data from each participants will be merged into two big sets of reference data and source data. Feature engineering is completed by feature data generator. Since I only evaluated the spread of the x and y coordinate of source data, the feature generator only calculates the standard deviation of input data. Once the training data is generated, I feed the data into WEKA and build models. I will provide more details in the data analysis section. 
% do I need to cite the website?

\par
The raw time frame data contains the length of lap time and the end of each lap. It has three types of data: the index of lap time, elapse of lap time, and the end of lap time in raw text. The raw time frame needs to be formatted into four columns: elapse, start time, end time, and confusion. Notice that I inserted some short time intervals ranging from 0.1 second to 0.4 second as time separators to separate the time interval for each step in the task. The elapse of time interval is used to distinguish the real time frame and the step separator. The start time and end time give the time frame for each step in the experiment tasks. If the subject reports confusion, then any time interval of executed steps of the task will be marked True in the confusion column. I wrote a formatter script in Python to process the raw time frame data. The formatter script turns the raw time interval data into the reference data that designate the start and end time in the eye tracker generated data. The training data generator will convert the start time and end time in reference data to the corresponding start and end index to extract preprocessed source data. 
\par
The raw data generated by the eye tracker needs to be preprocessed before becoming the input of training data generator.



\newpage
\begin{appendices}

\section{An appendix section}

\subsection{Previous Works}
Before working on this project, I was working on building a game agent capable of recognizing the intention of human players in pacman. However, due to lack of literature support, the attempt was unsuccessful, and I was forced to switch topic. After that, I was trying to build a match prediction model in CS:GO, but I had to switch topic again due to the same reason. Finally, I came up with the idea of building a adaptive user interface program in excel, and finalized the idea to this project.

\end{appendices}


\printbibliography{}


\end{document}
